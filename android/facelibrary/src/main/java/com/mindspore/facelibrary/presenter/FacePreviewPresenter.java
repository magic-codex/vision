/*
 * Copyright (c) 2022. Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindspore.facelibrary.presenter;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.opengl.EGLContext;
import android.view.View;
import android.view.animation.TranslateAnimation;

import androidx.annotation.NonNull;

import com.mindspore.enginelibrary.engine.FaceTracker;
import com.mindspore.facelibrary.R;
import com.mindspore.facelibrary.camera.CameraController;
import com.mindspore.facelibrary.camera.CameraParam;
import com.mindspore.facelibrary.camera.ICameraController;
import com.mindspore.facelibrary.ui.face.FaceMainActivity;

import org.jetbrains.annotations.NotNull;

public class FacePreviewPresenter extends PreviewPresenter<FaceMainActivity> {
    private FaceMainActivity mainActivity;

    private CameraParam mCameraParam;
    // 渲染器
//    private final CameraRenderer mCameraRenderer;

    // 相机接口
    private ICameraController mCameraController;

    public FacePreviewPresenter(FaceMainActivity target) {
        super(target);
        this.mainActivity = target;

        mCameraController = new CameraController(mainActivity);
//        mCameraController.setPreviewCallback(this);
//        mCameraController.setOnFrameAvailableListener(this);
//        mCameraController.setOnSurfaceTextureListener(this);

        // 初始化检测器
//        FaceTracker.getInstance()
//                .setFaceCallback(this)
//                .previewTrack(true)
//                .initTracker();
    }

    @Override
    public void onBindSharedContext(EGLContext context) {

    }

    @Override
    public void onSurfaceCreated(SurfaceTexture surfaceTexture) {

    }

    @Override
    public void onSurfaceChanged(int width, int height) {

    }

    @Override
    public void onSurfaceDestroyed() {

    }

    @Override
    public void changeDynamicFilter(int filterIndex) {

    }

    @Override
    public int previewFilter() {
        return 0;
    }

    @Override
    public int nextFilter() {
        return 0;
    }

    @Override
    public int getFilterIndex() {
        return 0;
    }

    @Override
    public void showCompare(boolean enable) {

    }

    @Override
    public void takePicture() {

    }

    @Override
    public void switchCamera() {

    }

    @Override
    public void startRecord() {

    }

    @Override
    public void stopRecord() {

    }

    @Override
    public void cancelRecord() {

    }

    @Override
    public boolean isRecording() {
        return false;
    }

    @Override
    public void changeFlashLight(boolean on) {

    }

    @Override
    public void enableEdgeBlurFilter(boolean enable) {

    }

    @Override
    public void setMusicPath(String path) {

    }

    @Override
    public void onOpenCameraSettingPage() {

    }

    @NonNull
    @NotNull
    @Override
    public Context getContext() {
        return mainActivity;
    }

    public void cameraBtnDown(View btnCamera) {
        AnimatorSet animator = (AnimatorSet) AnimatorInflater
                .loadAnimator(mainActivity, R.animator.camera_btn_fade_out);
        animator.setTarget(btnCamera);
        animator.start();
    }

    public void cameraBtnUp(View btnCamera) {
        AnimatorSet animator = (AnimatorSet) AnimatorInflater
                .loadAnimator(mainActivity, R.animator.camera_btn_fade_in);
        animator.setTarget(btnCamera);
        animator.start();
    }

    public void showFaceView(View frameLayout) {
        final TranslateAnimation ctrlAnimation = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_SELF, 0, TranslateAnimation.RELATIVE_TO_SELF, 0,
                TranslateAnimation.RELATIVE_TO_SELF, 1, TranslateAnimation.RELATIVE_TO_SELF, 0);
        ctrlAnimation.setDuration(200L);
        frameLayout.setVisibility(View.VISIBLE);
        frameLayout.startAnimation(ctrlAnimation);

    }

    public void closeFaceView(View frameLayout) {
        final TranslateAnimation ctrlAnimation = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_SELF, 0, TranslateAnimation.RELATIVE_TO_SELF, 0,
                TranslateAnimation.RELATIVE_TO_SELF, 0, TranslateAnimation.RELATIVE_TO_SELF, 1);
        ctrlAnimation.setDuration(200L);
        frameLayout.setVisibility(View.INVISIBLE);
        frameLayout.startAnimation(ctrlAnimation);
    }
}
