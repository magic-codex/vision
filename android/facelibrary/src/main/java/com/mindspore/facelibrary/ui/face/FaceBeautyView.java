/*
 * Copyright (c) 2022. Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindspore.facelibrary.ui.face;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mindspore.facelibrary.R;

import com.mindspore.facelibrary.event.FaceCloseEvent;
import com.mindspore.facelibrary.ui.adapter.FaceBeautyItemAdapter;
import com.mindspore.facelibrary.ui.bean.FaceBeautyItemBean;
import com.warkiz.widget.IndicatorSeekBar;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO: document your custom view class.
 */
public class FaceBeautyView extends LinearLayout implements View.OnClickListener {

    private Context mContext;
    private View mView;
    private RecyclerView horRecyclerView;

    public FaceBeautyView(Context context) {
        this(context, null);
    }

    public FaceBeautyView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FaceBeautyView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mContext = context;
        mView = LayoutInflater.from(mContext).inflate(R.layout.layout_face_beauty, this);
        init(mView);
    }

    private TextView seekText;
    private ImageView compareImage;
    private ImageView closeImage;
    private IndicatorSeekBar seekBar;
    private TextView btnBeauty,btnBody,btnMakeup;
    private TextView btnReset;

    private void init(View view) {
        seekText = view.findViewById(R.id.seek_text);
        compareImage = view.findViewById(R.id.img);
        seekBar = view.findViewById(R.id.seekBar);

        btnBeauty = view.findViewById(R.id.btn_beauty);
        btnBody = view.findViewById(R.id.btn_body);
        btnMakeup = view.findViewById(R.id.btn_makeup);
        closeImage = view.findViewById(R.id.close_btn);
        btnReset = view.findViewById(R.id.reset);

        compareImage.setOnClickListener(this);
        btnBeauty.setOnClickListener(this);
        btnBody.setOnClickListener(this);
        btnMakeup.setOnClickListener(this);
        closeImage.setOnClickListener(this);
        btnReset.setOnClickListener(this);

        List<FaceBeautyItemBean> itemList = new ArrayList<>();
        itemList.add(new FaceBeautyItemBean(1, R.drawable.panel_ic_classic_b, R.drawable.panel_ic_classic_p, "经典女神"));
        itemList.add(new FaceBeautyItemBean(2, R.drawable.panel_ic_long_b, R.drawable.panel_ic_long_p, "长脸专属"));
        itemList.add(new FaceBeautyItemBean(3, R.drawable.panel_ic_natural_b, R.drawable.panel_ic_natural_p, "自然脸"));
        itemList.add(new FaceBeautyItemBean(4, R.drawable.panel_ic_round_b, R.drawable.panel_ic_round_p, "圆脸专属"));
        itemList.add(new FaceBeautyItemBean(5, R.drawable.panel_ic_classic_b, R.drawable.panel_ic_classic_p, "经典女神"));
        itemList.add(new FaceBeautyItemBean(6, R.drawable.panel_ic_long_b, R.drawable.panel_ic_long_p, "长脸专属"));
        itemList.add(new FaceBeautyItemBean(7, R.drawable.panel_ic_natural_b, R.drawable.panel_ic_natural_p, "自然脸"));
        itemList.add(new FaceBeautyItemBean(8, R.drawable.panel_ic_round_b, R.drawable.panel_ic_round_p, "圆脸专属"));

        seekText.setText(itemList.get(0).getName());

        horRecyclerView = view.findViewById(R.id.horRecyclerView);
        horRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL,false));
        FaceBeautyItemAdapter adapter = new FaceBeautyItemAdapter(mContext,itemList);
        adapter.setmOnClickListener(position -> {
            seekText.setText(itemList.get(position).getName());
            adapter.setSelectPosition(position);
            adapter.notifyDataSetChanged();
        });
        horRecyclerView.setAdapter(adapter);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.close_btn) {
            FaceCloseEvent faceCloseEvent = new FaceCloseEvent();
            EventBus.getDefault().post(faceCloseEvent);
        }else if (v.getId() == R.id.btn_beauty){
            btnBeauty.setTextColor(getResources().getColor(R.color.colorPrimary));
            btnBody.setTextColor(getResources().getColor(R.color.gray_face_text));
            btnMakeup.setTextColor(getResources().getColor(R.color.gray_face_text));
        }else if (v.getId() == R.id.btn_body){
            btnBeauty.setTextColor(getResources().getColor(R.color.gray_face_text));
            btnBody.setTextColor(getResources().getColor(R.color.colorPrimary));
            btnMakeup.setTextColor(getResources().getColor(R.color.gray_face_text));
        }else if (v.getId() == R.id.btn_makeup){
            btnBeauty.setTextColor(getResources().getColor(R.color.gray_face_text));
            btnBody.setTextColor(getResources().getColor(R.color.gray_face_text));
            btnMakeup.setTextColor(getResources().getColor(R.color.colorPrimary));
        }
    }

}