/*
 * Copyright (c) 2022. Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindspore.facelibrary.ui.face;

import android.graphics.SurfaceTexture;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageCapture;

import com.mindspore.enginelibrary.engine.FaceTracker;
import com.mindspore.facelibrary.R;
import com.mindspore.facelibrary.camera.CameraParam;
import com.mindspore.facelibrary.event.FaceCloseEvent;
import com.mindspore.facelibrary.presenter.FacePreviewPresenter;
import com.mindspore.facelibrary.ui.widget.CameraTextureView;
import com.mindspore.utilslibrary.ui.BaseActivity;
import com.mindspore.utilslibrary.ui.view.UpImageDownTextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class FaceMainActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "FaceMainActivity";

    private FacePreviewPresenter facePreviewPresenter;
    private CameraTextureView cameraTextureView;
    private UpImageDownTextView classBtn, detectionBtn, faceBtn, filterBtn;
    private ImageButton btnCamera;
    private FrameLayout frameLayout;


    private FaceBeautyView faceBeautyView;
    private FaceStickerView faceStickerView;

    // 预览参数
    private CameraParam mCameraParam;


    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        facePreviewPresenter.onDestroy();
        facePreviewPresenter = null;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(FaceCloseEvent event) {
        if (frameLayout.getVisibility() == View.VISIBLE) {
            facePreviewPresenter.closeFaceView(frameLayout);
            facePreviewPresenter.cameraBtnUp(btnCamera);
        }
    }

    @Override
    protected void init() {
        mCameraParam = CameraParam.getInstance();
        facePreviewPresenter = new FacePreviewPresenter(this);

        initView();
        faceTrackerRequestNetwork();
    }


    private void initView() {
        ImageView closeBtn = findViewById(R.id.closeBtn);
        ImageView moreBtn = findViewById(R.id.moreBtn);
        btnCamera = findViewById(R.id.btnCamera1);
        cameraTextureView = findViewById(R.id.viewFinder);
        ImageView switchBtn = findViewById(R.id.switchBtn);
        ImageView galleryBtn = findViewById(R.id.galleryBtn);
        classBtn = findViewById(R.id.btnOne);
        detectionBtn = findViewById(R.id.btnTwo);
        faceBtn = findViewById(R.id.btnThree);
        filterBtn = findViewById(R.id.btnFour);
        frameLayout = findViewById(R.id.f3);
        closeBtn.setOnClickListener(this);
        moreBtn.setOnClickListener(this);
        btnCamera.setOnClickListener(this);
        switchBtn.setOnClickListener(this);
        galleryBtn.setOnClickListener(this);
        cameraTextureView.setOnClickListener(this);
        classBtn.setOnClickListener(this);
        detectionBtn.setOnClickListener(this);
        faceBtn.setOnClickListener(this);
        filterBtn.setOnClickListener(this);

        cameraTextureView.addOnTouchScroller(mTouchScroller);
        cameraTextureView.addMultiClickListener(mMultiClickListener);
        cameraTextureView.setSurfaceTextureListener(mSurfaceTextureListener);

    }

    @Override
    public int getLayout() {
        return R.layout.activity_face_main;
    }

    /**
     * 人脸检测SDK验证，可以替换成自己的SDK
     */
    private void faceTrackerRequestNetwork() {
        new Thread(() -> FaceTracker.requestFaceNetwork(this)).start();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnOne) {
            if (faceBeautyView == null) {
                faceBeautyView = new FaceBeautyView(this);
            }
            frameLayout.removeAllViews();
            frameLayout.addView(faceBeautyView);

            facePreviewPresenter.showFaceView(frameLayout);
            facePreviewPresenter.cameraBtnDown(btnCamera);
        } else if (v.getId() == R.id.btnTwo) {
            if (faceStickerView == null) {
                faceStickerView = new FaceStickerView(this);
            }
            frameLayout.removeAllViews();
            frameLayout.addView(faceStickerView);

            facePreviewPresenter.showFaceView(frameLayout);
            facePreviewPresenter.cameraBtnDown(btnCamera);

        } else if (v.getId() == R.id.viewFinder) {
            if (frameLayout.getVisibility() == View.VISIBLE) {
                facePreviewPresenter.closeFaceView(frameLayout);
                facePreviewPresenter.cameraBtnUp(btnCamera);
            }
        }

    }


    // ------------------------------- TextureView 滑动、点击回调 ----------------------------------
    private CameraTextureView.OnTouchScroller mTouchScroller = new CameraTextureView.OnTouchScroller() {

        @Override
        public void swipeBack() {
            facePreviewPresenter.nextFilter();
        }

        @Override
        public void swipeFrontal() {
            facePreviewPresenter.previewFilter();
        }

        @Override
        public void swipeUpper(boolean startInLeft, float distance) {
            Log.d(TAG, "swipeUpper, startInLeft ? " + startInLeft + ", distance = " + distance);

        }

        @Override
        public void swipeDown(boolean startInLeft, float distance) {
            Log.d(TAG, "swipeDown, startInLeft ? " + startInLeft + ", distance = " + distance);

        }

    };

    /**
     * 单双击回调监听
     */
    private CameraTextureView.OnMultiClickListener mMultiClickListener = new CameraTextureView.OnMultiClickListener() {

        @Override
        public void onSurfaceSingleClick(final float x, final float y) {
//            // 处理浮窗Fragment
//            if (onBackPressed()) {
//                return;
//            }
//
//            // 如果处于触屏拍照状态，则直接拍照，不做对焦处理
//            if (mCameraParam.touchTake) {
//                takePicture();
//                return;
//            }


        }

        @Override
        public void onSurfaceDoubleClick(float x, float y) {
//            switchCamera();
        }

    };

    // ---------------------------- TextureView SurfaceTexture监听 ---------------------------------
    private TextureView.SurfaceTextureListener mSurfaceTextureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            facePreviewPresenter.onSurfaceCreated(surface);
            facePreviewPresenter.onSurfaceChanged(width, height);
            Log.d(TAG, "onSurfaceTextureAvailable: ");
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
            facePreviewPresenter.onSurfaceChanged(width, height);
            Log.d(TAG, "onSurfaceTextureSizeChanged: ");
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            facePreviewPresenter.onSurfaceDestroyed();
            Log.d(TAG, "onSurfaceTextureDestroyed: ");
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {

        }
    };


}