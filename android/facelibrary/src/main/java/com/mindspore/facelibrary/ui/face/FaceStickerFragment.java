/*
 * Copyright (c) 2022. Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mindspore.facelibrary.ui.face;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mindspore.facelibrary.R;
import com.mindspore.facelibrary.ui.adapter.FaceStickerItemAdapter;
import com.mindspore.facelibrary.ui.bean.FaceBeautyItemBean;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FaceStickerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FaceStickerFragment extends Fragment {


    public FaceStickerFragment() {
        // Required empty public constructor
    }


    public static FaceStickerFragment newInstance() {
        FaceStickerFragment fragment = new FaceStickerFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_face_sticker, container, false);
        recyclerView = view.findViewById(R.id.recycleView);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),5));
        List<FaceBeautyItemBean> itemList = new ArrayList<>();
        itemList.add(new FaceBeautyItemBean(1, R.drawable.sticker1, R.drawable.sticker1, "经典女神"));
        itemList.add(new FaceBeautyItemBean(2, R.drawable.sticker2, R.drawable.sticker2, "长脸专属"));
        itemList.add(new FaceBeautyItemBean(3, R.drawable.sticker3, R.drawable.sticker3, "自然脸"));
        itemList.add(new FaceBeautyItemBean(4, R.drawable.sticker4, R.drawable.sticker4, "圆脸专属"));
        itemList.add(new FaceBeautyItemBean(5, R.drawable.sticker5, R.drawable.sticker5, "经典女神"));
        itemList.add(new FaceBeautyItemBean(6, R.drawable.sticker6, R.drawable.sticker6, "长脸专属"));
        itemList.add(new FaceBeautyItemBean(7, R.drawable.sticker7, R.drawable.sticker7, "自然脸"));
        itemList.add(new FaceBeautyItemBean(8, R.drawable.sticker8, R.drawable.sticker8, "圆脸专属"));
        itemList.add(new FaceBeautyItemBean(8, R.drawable.sticker9, R.drawable.sticker8, "圆脸专属"));
        itemList.add(new FaceBeautyItemBean(1, R.drawable.sticker1, R.drawable.sticker1, "经典女神"));
        itemList.add(new FaceBeautyItemBean(2, R.drawable.sticker2, R.drawable.sticker2, "长脸专属"));
        itemList.add(new FaceBeautyItemBean(3, R.drawable.sticker3, R.drawable.sticker3, "自然脸"));
        itemList.add(new FaceBeautyItemBean(4, R.drawable.sticker4, R.drawable.sticker4, "圆脸专属"));
        itemList.add(new FaceBeautyItemBean(5, R.drawable.sticker5, R.drawable.sticker5, "经典女神"));
        itemList.add(new FaceBeautyItemBean(6, R.drawable.sticker6, R.drawable.sticker6, "长脸专属"));
        itemList.add(new FaceBeautyItemBean(7, R.drawable.sticker7, R.drawable.sticker7, "自然脸"));
        itemList.add(new FaceBeautyItemBean(8, R.drawable.sticker8, R.drawable.sticker8, "圆脸专属"));

        adapter = new FaceStickerItemAdapter(getContext(), itemList);
        adapter.setmOnClickListener(position -> {
            adapter.setSelectPosition(position);
            adapter.notifyDataSetChanged();
        });
        recyclerView.setAdapter(adapter);
        return view;
    }

    RecyclerView recyclerView;
    FaceStickerItemAdapter adapter;
}