# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""models module, import every backbone, neck, head to register classes"""
from mindvision.msdetection.models.backbone.darknet import CspDarkNet, DarkNet
from mindvision.msdetection.models.backbone.yolov5_backbone import YOLOv5Backbone
from mindvision.msdetection.models.backbone.mobilenet_v1 import MobileNetV1

from mindvision.msdetection.models.detector.faster_rcnn import FasterRCNN
from mindvision.msdetection.models.detector.yolo import YOLOv5, YOLOv4, YOLOv3

from mindvision.msdetection.models.head.roi_head import StandardRoIHead
from mindvision.msdetection.models.head.rpn import RPNHead
from mindvision.msdetection.models.head.yolo_head import YOLOv5Head, YOLOv4Head, YOLOv3Head

from mindvision.msdetection.models.neck.fpn import FPN
from mindvision.msdetection.models.neck.yolo_neck import YOLOv5Neck, YOLOv4Neck, YOLOv3Neck
from mindvision.msdetection.models.neck.mobilenetv1_fpn_neck import MobileNetV1FPN
from mindvision.msdetection.models.meta_arch.train_wrapper import TrainingWrapper, TrainingWrapperssd

from mindvision.msdetection.models.backbone.resnext import ResNeXt
from mindvision.msdetection.models.detector.mask_rcnn import MaskRCNN
from mindvision.msdetection.models.detector.retinanet import RetinaNet
from mindvision.msdetection.models.head.retina_head import RetinaHead
from mindvision.msdetection.models.neck.retina_fpn import RetinaFPN
from mindvision.msdetection.models.detection_engine.common_bbox2image import CommonDetectionEngine
from mindvision.msdetection.models.detection_engine.retinanet_bbox2image import RetinaDetectionEngine
from mindvision.msdetection.models.detection_engine.yolov5_bbbox2image import YOLOv5DetectionEngine
from mindvision.msdetection.models.detection_engine.ssd_bbox2image import SsdDetectionEngine

from mindvision.msdetection.models.detector.centerface import CenterFace
from mindvision.msdetection.models.backbone.mobile_v2 import MobileNetV2
from mindvision.msdetection.models.head.centerface_head import CenterFaceHead
from mindvision.msdetection.models.detection_engine.centerface_detector import CenterfaceDetectionEngine
from mindvision.msdetection.models.neck.centerface_neck import CenterFaceNeck
from mindvision.msdetection.models.head.ssd_head import WeightSharedMultiBox
from mindvision.msdetection.models.detector.retinaface import RetinaFace
from mindvision.msdetection.models.head.retinaface_head import RetinaFaceHead
from mindvision.msdetection.models.neck.retinaface_neck import RetinaFaceNeck
from mindvision.msdetection.models.detection_engine.retinaface_bbox2image import RetinafaceDetectionEngine
