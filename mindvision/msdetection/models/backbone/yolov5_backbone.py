# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
""" Backbone for YOLOv5"""

from typing import Optional

import mindspore.nn as nn
from mindspore import ops

from mindvision.engine.class_factory import ClassFactory, ModuleType
from mindvision.classification.models.blocks.blocks import ConvNormActivation
from mindvision.msdetection.models.block.yolo_blocks import CSPBlock, SPPBlock
from mindvision.classification.engine.ops.swish import Swish


class Focus(nn.Cell):
    """
    Focus structure used in YOLOv5 to change shape of the input image. Such as: an image
    with 608*608*3 shape will be change to 304*304*12 at first. Then use a CBL block to
    process it.

    Args:
        in_channel (int): The dimension of input tensor.
        out_channel (int): The dimension of output tensor.
        kernel_size (int): Convolutional kernel size of CBL block.
        stride (int): The movement stride of the 2D convolution kernel in CBL block.
        activation_func (nn.Cell, optional): To define the activation function
            of the 2D convolution kernel in CBL block. Default: Swish.

    Returns:
        Tensor, output tensor.

    Examples:
        >>> ops = Focus(3, 32, 1, 1, Swish)
    """

    def __init__(self,
                 in_channel: int,
                 out_channel: int,
                 kernel_size: int = 1,
                 strid: int = 1,
                 activation_func: Optional[nn.Cell] = Swish):
        super(Focus, self).__init__()

        self.cbl = ConvNormActivation(in_channel * 4,
                                      out_channel,
                                      kernel_size=kernel_size,
                                      stride=strid,
                                      activation=activation_func)
        self.concat = ops.Concat(axis=1)

    def construct(self, x):
        y = self.concat((x[..., ::2, ::2], x[..., 1::2, ::2],
                         x[..., ::2, 1::2], x[..., 1::2, 1::2]))
        return self.cbl(y)


@ClassFactory.register(ModuleType.BACKBONE)
class YOLOv5Backbone(nn.Cell):
    """
    YOLOv5 backbone.

    Args:
        net_width (tuple): With this tuple, the numbers of convolutional kernel will be set
            among the CBL and CSP blocks. Default: (32, 64, 128, 256, 512).
        net_depth (tuple): With this tuple, the numbers of Bottleneck will be set
            among CSP blocks. Default: (1, 3, 3, 1).
        spp_parameter (tuple): Kernel size will be used in SPP block. Default: (5, 9, 13).

    Inputs:
        - **x** (Tensor) - Tensor of shape :math:`(N, C_{in}, H_{in}, W_{in})`. C_ should be 3.

    Outputs:
        - y1, y2, y3 (Tensor, Tensor, Tensor)
        - Tensor of shape :math:`(N, 128, 76, 76), (N, 256, 38, 38), (N, 512, 19, 19)`.

    Supported Platforms:
        ``GPU``

    Examples:
        >>> net = YOLOv5Backbone((32, 64, 128, 256, 512), (1, 3, 3, 1), (5, 9, 13))
        >>> x = ms.Tensor(np.ones([1, 3, 608, 608]), ms.float32)
        >>> y1, y2, y3 = net(x)
        >>> print(y1.shape)
        >>> print(y2.shape)
        >>> print(y3.shape)
        (1, 128, 76, 76)
        (1, 256, 38, 38)
        (1, 512, 19, 19)

    About YOLOv5:

    The YOLOv5 is a famous object detection model. It mainly includes backbone, neck and detection part.
    These code mainly implements the backbone part. The output will be used as input of neck.
    The backbone structure draw lessons from many other structure, such as: CSP, SPP.
    """

    def __init__(self,
                 net_width: tuple = (32, 64, 128, 256, 512),
                 net_depth: tuple = (1, 3, 3, 1),
                 spp_parameter: tuple = (5, 9, 13)):
        super(YOLOv5Backbone, self).__init__()

        self.focus = Focus(3, net_width[0], kernel_size=3, strid=1)
        self.cbl1 = ConvNormActivation(net_width[0], net_width[1],
                                       kernel_size=3, stride=2,
                                       activation=Swish)
        self.csp1_1 = CSPBlock(net_width[1], net_width[1], num_subblock=net_depth[0])

        self.cbl2 = ConvNormActivation(net_width[1], net_width[2],
                                       kernel_size=3, stride=2,
                                       activation=Swish)
        self.csp1_2 = CSPBlock(net_width[2], net_width[2], num_subblock=net_depth[1])

        self.cbl3 = ConvNormActivation(net_width[2], net_width[3],
                                       kernel_size=3, stride=2,
                                       activation=Swish)
        self.csp1_3 = CSPBlock(net_width[3], net_width[3], num_subblock=net_depth[2])

        self.cbl4 = ConvNormActivation(net_width[3], net_width[4],
                                       kernel_size=3, stride=2,
                                       activation=Swish)
        self.spp = SPPBlock(net_width[4], net_width[4], pooling_list=spp_parameter)

        self.csp2_1 = CSPBlock(net_width[4], net_width[4], num_subblock=net_depth[3], shortcut=False)

    def construct(self, x):
        """YOLOv5 Backbone structure method"""
        focus_output = self.focus(x)
        cbl_output1 = self.cbl1(focus_output)
        csp_output1 = self.csp1_1(cbl_output1)

        cbl_output2 = self.cbl2(csp_output1)
        csp_output2 = self.csp1_2(cbl_output2)

        cbl_output3 = self.cbl3(csp_output2)
        csp_output3 = self.csp1_3(cbl_output3)

        cbl_output4 = self.cbl4(csp_output3)
        spp1 = self.spp(cbl_output4)
        csp_output4 = self.csp2_1(spp1)

        return csp_output2, csp_output3, csp_output4
