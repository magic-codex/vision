# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Blocks used in YOLO network"""

from mindspore import nn
from mindspore import ops

from mindvision.classification.models.blocks import ResidualCell
from mindvision.classification.models.blocks.blocks import ConvNormActivation
from mindvision.classification.engine.ops.swish import Swish


class Bottleneck(nn.Cell):
    """
    Standard bottleneck used in YOLOv5 implementation.

    Args:
        in_channel (int): The dimension of input tensor.
        out_channel (int): The dimension of output tensor.
        shortcut (bool): Whether to add residual connect module. Default: True.
        scale (float): To define the dimension in the bottleneck, value between 0 and 1.
            Default: 0.5.

    Returns:
        Tensor, output tensor.

    Examples:
        >>> ops = Bottleneck(64, 128, True, 0.5)
    """

    def __init__(self,
                 in_channel: int,
                 out_channel: int,
                 shortcut: bool = True,
                 scale: float = 0.5):
        super(Bottleneck, self).__init__()

        mid_channel = int(out_channel * scale)
        self.bottleneck = nn.SequentialCell(
            ConvNormActivation(in_channel, mid_channel,
                               kernel_size=1, stride=1,
                               activation=Swish),
            ConvNormActivation(mid_channel, out_channel,
                               kernel_size=3, stride=1,
                               activation=Swish)
        )
        self.add = shortcut and in_channel == out_channel
        self.residule = ResidualCell(self.bottleneck)

    def construct(self, x):
        if self.add:
            return self.residule(x)
        return self.bottleneck(x)


class CSPBlockV2(nn.Cell):
    """
    Another CSP Block used in YOLOv5. This block includes 2 CBL block, 2 convolutional layers some residual blocks
    which defined by num_subblock, if you set shortcut as False, the residual block will be replaced by CBL block.
    It means that this CSP block has two version that will be used in backbone and neck of Yolov5.

    Args:
        in_channel (int): The dimension of input tensor.
        out_channel (int): The dimension of output tensor.
        num_subblock (int): To define the repeat times of residual block, value should above 0.
            Default: 1.
        shortcut (bool): Whether to add residual connect module. Default: True.

    Returns:
        Tensor, output tensor.

    Examples:
        >>> ops = CSPBlockV2(64, 128, 3, False)
    """

    def __init__(self,
                 in_channel: int,
                 out_channel: int,
                 num_subblock: int = 1,
                 shortcut=True):
        super(CSPBlockV2, self).__init__()

        mid_channel = int(out_channel * 0.5)
        self.cbl1 = ConvNormActivation(in_channel, mid_channel,
                                       kernel_size=1, stride=1,
                                       activation=Swish)
        self.cbl2 = ConvNormActivation(mid_channel * 2, out_channel,
                                       kernel_size=1, stride=1,
                                       activation=Swish)

        self.conv1 = nn.Conv2d(in_channel, mid_channel, 1, 1, has_bias=False)
        self.conv2 = nn.Conv2d(mid_channel, mid_channel, 1, 1, has_bias=False)

        self.bn = nn.BatchNorm2d(2 * mid_channel, momentum=0.9, eps=1e-5)
        self.activation = nn.LeakyReLU(0.1)
        self.res_unit = nn.SequentialCell([Bottleneck(mid_channel, mid_channel, shortcut, scale=1.0)
                                           for _ in range(num_subblock)])
        self.concat = ops.Concat(axis=1)

    def construct(self, x):
        y1 = self.conv2(self.res_unit(self.cbl1(x)))
        y2 = self.conv1(x)
        y3 = self.concat((y1, y2))

        return self.cbl2(self.activation(self.bn(y3)))


class CSPBlock(nn.Cell):
    """
    CSP Block used in YOLOv5. This block includes 3 CBL block, some residual blocks which defined by num_subblock.
    If you set shortcut as False, the residual block will be replaced by CBL block.
    It means that this CSP block has two version that will be used in backbone and neck of Yolov5.

    Args:
        in_channel (int): The dimension of input tensor.
        out_channel (int): The dimension of output tensor.
        num_subblock (int): To define the repeat times of residual block, value should above 0.
            Default: 1.
        shortcut (bool): Whether to add residual connect module. Default: True.

    Returns:
        Tensor, output tensor.

    Examples:
        >>> ops = CSPBlock(64, 128, 3, True)
    """

    def __init__(self,
                 in_channel: int,
                 out_channel: int,
                 num_subblock: int = 1,
                 shortcut: bool = True):
        super(CSPBlock, self).__init__()
        mid_channel = int(out_channel * 0.5)

        self.cbl1 = ConvNormActivation(in_channel, mid_channel,
                                       kernel_size=1, stride=1,
                                       activation=Swish)
        self.cbl2 = ConvNormActivation(in_channel, mid_channel,
                                       kernel_size=1, stride=1,
                                       activation=Swish)
        self.cbl3 = ConvNormActivation(mid_channel * 2, out_channel,
                                       kernel_size=1, stride=1,
                                       activation=Swish)

        self.res_unit = nn.SequentialCell([Bottleneck(mid_channel, mid_channel, shortcut, scale=1.0)
                                           for _ in range(num_subblock)])
        self.concat = ops.Concat(axis=1)

    def construct(self, x):
        y1 = self.res_unit(self.cbl1(x))
        y2 = self.cbl2(x)
        y3 = self.concat((y1, y2))

        return self.cbl3(y3)


class SPPBlock(nn.Cell):
    """
    Spatial pyramid pooling layer used in YOLOv5 also called SPP block.

    Args:
        in_channel (int): The dimension of input tensor.
        out_channel (int): The dimension of output tensor.
        pooling_list (tuple): Define the kernel size of other 3 pooling channel.
        Default: (5, 9, 13).

    Returns:
        Tensor, output tensor.

    Examples:
        >>> ops = SPPBlock(512, 1024, (5, 9, 13))
    """

    def __init__(self,
                 in_channel: int,
                 out_channel: int,
                 pooling_list: tuple = (5, 9, 13)):
        super(SPPBlock, self).__init__()
        assert len(pooling_list) == 3, "The length of pooling_list should be 3."

        mid_channel = in_channel // 2
        self.cbl1 = ConvNormActivation(in_channel, mid_channel,
                                       kernel_size=1, stride=1,
                                       activation=Swish)
        self.cbl2 = ConvNormActivation(mid_channel * (len(pooling_list) + 1), out_channel,
                                       kernel_size=1, stride=1,
                                       activation=Swish)

        self.maxpool1 = nn.MaxPool2d(kernel_size=pooling_list[0], stride=1, pad_mode='same')
        self.maxpool2 = nn.MaxPool2d(kernel_size=pooling_list[1], stride=1, pad_mode='same')
        self.maxpool3 = nn.MaxPool2d(kernel_size=pooling_list[2], stride=1, pad_mode='same')
        self.concat = ops.Concat(axis=1)

    def construct(self, x):
        x = self.cbl1(x)

        pool1 = self.maxpool1(x)
        pool2 = self.maxpool2(x)
        pool3 = self.maxpool3(x)

        concatpool = self.concat((x, pool1, pool2, pool3))

        return self.cbl2(concatpool)
