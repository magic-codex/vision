# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
""" YOLO neck implement"""

from mindspore import nn
from mindspore import ops

from mindvision.engine.class_factory import ClassFactory, ModuleType
from mindvision.msdetection.models.backbone.yolov5_backbone import CSPBlock
from mindvision.classification.models.blocks.blocks import ConvNormActivation
from mindvision.classification.engine.ops.swish import Swish


class YoloBlock(nn.Cell):
    """YoloBlock for YOLO.

    Args:
        in_channels: Integer. Input channel.
        out_chs: Integer. Middle channel.
        out_channels: Integer. Output channel.

    Returns:
        Tuple, tuple of output tensor,(f1,f2,f3).

    Examples:
        YoloBlock(1024, 512, 255)

    """

    def __init__(self, in_channels, out_chs, out_channels):
        super(YoloBlock, self).__init__()
        out_chs_2 = out_chs * 2

        self.conv0 = ConvNormActivation(in_channels, out_chs, kernel_size=1)
        self.conv1 = ConvNormActivation(out_chs, out_chs_2, kernel_size=3)

        self.conv2 = ConvNormActivation(out_chs_2, out_chs, kernel_size=1)
        self.conv3 = ConvNormActivation(out_chs, out_chs_2, kernel_size=3)

        self.conv4 = ConvNormActivation(out_chs_2, out_chs, kernel_size=1)
        self.conv5 = ConvNormActivation(out_chs, out_chs_2, kernel_size=3)

        self.conv6 = nn.Conv2d(out_chs_2, out_channels, kernel_size=1,
                               stride=1, has_bias=True)

    def construct(self, x):
        """Construct of YoloBlock."""
        c1 = self.conv0(x)
        c2 = self.conv1(c1)

        c3 = self.conv2(c2)
        c4 = self.conv3(c3)

        c5 = self.conv4(c4)
        c6 = self.conv5(c5)

        out = self.conv6(c6)
        return c5, out


@ClassFactory.register(ModuleType.NECK)
class YOLOv3Neck(nn.Cell):
    """The neck of YOLOv3.

    Note:
         backbone = darknet53

     Args:
         backbone_shape: List. Darknet output channels shape.
         out_channel: Integer. Output channel.

     Returns:
         Tensor, output tensor.

     Examples:
         YOLOv3Neck(backbone_shape=[64, 128, 256, 512, 1024]
                backbone=darknet53(),
                out_channel=255)
    """

    def __init__(self, backbone_shape, out_channel):
        super(YOLOv3Neck, self).__init__()
        self.out_channel = out_channel
        self.back_block0 = YoloBlock(backbone_shape[-1],
                                     out_chs=backbone_shape[-2],
                                     out_channels=out_channel)

        self.conv1 = ConvNormActivation(in_planes=backbone_shape[-2],
                                        out_planes=backbone_shape[-2] // 2,
                                        kernel_size=1)
        self.back_block1 = YoloBlock(
            in_channels=backbone_shape[-2] + backbone_shape[-3],
            out_chs=backbone_shape[-3],
            out_channels=out_channel
        )

        self.conv2 = ConvNormActivation(in_planes=backbone_shape[-3],
                                        out_planes=backbone_shape[-3] // 2,
                                        kernel_size=1)
        self.back_block2 = YoloBlock(
            in_channels=backbone_shape[-3] + backbone_shape[-4],
            out_chs=backbone_shape[-4],
            out_channels=out_channel
        )
        self.concat = ops.Concat(axis=1)

    def construct(self, x):
        """Construct of YOLOv3Neck."""
        feature_map3, feature_map2, feature_map1 = x[2], x[1], x[0]
        img_height = ops.Shape()(feature_map1)[2] * 8
        img_width = ops.Shape()(feature_map1)[3] * 8

        con1, big_object_output = self.back_block0(feature_map3)

        con1 = self.conv1(con1)
        ups1 = ops.ResizeNearestNeighbor(
            (img_height // 16, img_width // 16))(con1)
        con1 = self.concat((ups1, feature_map2))
        con2, medium_object_output = self.back_block1(con1)

        con2 = self.conv2(con2)
        ups2 = ops.ResizeNearestNeighbor((img_height // 8, img_width // 8))(con2)
        con3 = self.concat((ups2, feature_map1))
        _, small_object_output = self.back_block2(con3)

        return big_object_output, medium_object_output, small_object_output


@ClassFactory.register(ModuleType.NECK)
class YOLOv4Neck(nn.Cell):
    """The neck of yolov4

     Note:
         backbone = CspDarkNet53

     Args:
         backbone_shape: List. Darknet output channels shape.
         backbone: Cell. Backbone Network.
         out_channel: Integer. Output channel.

     Returns:
         Tensor, output tensor.

     Examples:
         YOLOv4(feature_shape=[1,3,416,416],
                backbone_shape=[64, 128, 256, 512, 1024]
                backbone=CspDarkNet53(),
                out_channel=255)
     """

    def __init__(self, backbone_shape, out_channel):
        super(YOLOv4Neck, self).__init__()
        self.out_channel = out_channel

        self.conv1 = ConvNormActivation(1024, 512, kernel_size=1)
        self.conv2 = ConvNormActivation(512, 1024, kernel_size=3)
        self.conv3 = ConvNormActivation(1024, 512, kernel_size=1)

        self.maxpool1 = nn.MaxPool2d(kernel_size=5, stride=1, pad_mode='same')
        self.maxpool2 = nn.MaxPool2d(kernel_size=9, stride=1, pad_mode='same')
        self.maxpool3 = nn.MaxPool2d(kernel_size=13, stride=1, pad_mode='same')
        self.conv4 = ConvNormActivation(2048, 512, kernel_size=1)

        self.conv5 = ConvNormActivation(512, 1024, kernel_size=3)
        self.conv6 = ConvNormActivation(1024, 512, kernel_size=1)
        self.conv7 = ConvNormActivation(512, 256, kernel_size=1)

        self.conv8 = ConvNormActivation(512, 256, kernel_size=1)
        self.back_block0 = YoloBlock(backbone_shape[-2],
                                     out_chs=backbone_shape[-3],
                                     out_channels=out_channel)

        self.conv9 = ConvNormActivation(256, 128, kernel_size=1)
        self.conv10 = ConvNormActivation(256, 128, kernel_size=1)
        self.conv11 = ConvNormActivation(128, 256, kernel_size=3, stride=2)
        self.conv12 = ConvNormActivation(256, 512, kernel_size=3, stride=2)

        self.back_block1 = YoloBlock(backbone_shape[-3],
                                     out_chs=backbone_shape[-4],
                                     out_channels=out_channel)
        self.back_block2 = YoloBlock(backbone_shape[-2],
                                     out_chs=backbone_shape[-3],
                                     out_channels=out_channel)
        self.back_block3 = YoloBlock(backbone_shape[-1],
                                     out_chs=backbone_shape[-2],
                                     out_channels=out_channel)

        self.concat = ops.Concat(axis=1)

    def construct(self, x):
        """
        x is the feature maps (f3, f2, f1)
        feature_map1 is (batch_size, backbone_shape[2], h/8, w/8)
        feature_map2 is (batch_size, backbone_shape[3], h/16, w/16)
        feature_map3 is (batch_size, backbone_shape[4], h/32, w/32)
        """
        feature_map3, feature_map2, feature_map1 = x[2], x[1], x[0]

        img_height = ops.Shape()(feature_map1)[2] * 8
        img_width = ops.Shape()(feature_map1)[3] * 8

        con1 = self.conv1(feature_map3)
        con2 = self.conv2(con1)
        con3 = self.conv3(con2)

        m1 = self.maxpool1(con3)
        m2 = self.maxpool2(con3)
        m3 = self.maxpool3(con3)
        spp = self.concat((m3, m2, m1, con3))
        con4 = self.conv4(spp)

        con5 = self.conv5(con4)
        con6 = self.conv6(con5)
        con7 = self.conv7(con6)

        ups1 = ops.ResizeNearestNeighbor((img_height // 16, img_width // 16))(con7)
        con8 = self.conv8(feature_map2)
        con9 = self.concat((ups1, con8))
        con10, _ = self.back_block0(con9)
        con11 = self.conv9(con10)
        ups2 = ops.ResizeNearestNeighbor((img_height // 8, img_width // 8))(con11)
        con12 = self.conv10(feature_map1)
        con13 = self.concat((ups2, con12))
        con14, small_object_output = self.back_block1(con13)

        con15 = self.conv11(con14)
        con16 = self.concat((con15, con10))
        con17, medium_object_output = self.back_block2(con16)

        con18 = self.conv12(con17)
        con19 = self.concat((con18, con6))
        _, big_object_output = self.back_block3(con19)
        return big_object_output, medium_object_output, small_object_output


@ClassFactory.register(ModuleType.NECK)
class YOLOv5Neck(nn.Cell):
    """
    The neck of YOLOv5

     Args:
         backbone_shape (tuple): The network width of YOLOv5Backbone. Default: (32, 64, 128, 256, 512).
         out_channel (int): The Dimension of output.

     Inputs:
        - **(x1, x2, x3)** (Tensor, Tensor, Tensor)
        - Each tensor of shape :math:`(N, C_{in}, H_{in}, W_{in})`.

     Outputs:
        - y1, y2, y3 (Tensor)
        - Tensor of shape :math:`(N, 255, 76, 76), (N, 255, 38, 38), (N, 255, 19, 19)`.

     Examples:
        >>> net = YOLOv5Neck((32, 64, 128, 256, 512), 255)
        >>> x1 = ms.Tensor(np.ones([1, 512, 19, 19]), ms.float32)
        >>> x2 = ms.Tensor(np.ones([1, 256, 38, 38]), ms.float32)
        >>> x3 = ms.Tensor(np.ones([1, 128, 76, 76]), ms.float32)
        >>> y1, y2, y3 = net((x1, x2, x3))
        >>> print(y1.shape)
        >>> print(y2.shape)
        >>> print(y3.shape)
        (1, 255, 76, 76)
        (1, 255, 38, 38)
        (1, 255, 19, 19)

        About YOLOv5Neck:

        The Neck part include FPN and PAN two parts. FPN is an upsampling process and
        PAN is a downsampling process. They are mainly for feature comfusion and get the
        output matrix.
     """

    def __init__(self,
                 backbone_shape: tuple = (32, 64, 128, 256, 512),
                 out_channel: int = 255):
        super(YOLOv5Neck, self).__init__()

        self.cbl1 = ConvNormActivation(backbone_shape[4], backbone_shape[3],
                                       kernel_size=1, stride=1,
                                       activation=Swish)
        self.csp2_1 = CSPBlock(backbone_shape[4], backbone_shape[3], num_subblock=1, shortcut=False)

        self.cbl2 = ConvNormActivation(backbone_shape[3], backbone_shape[2],
                                       kernel_size=1, stride=1,
                                       activation=Swish)
        self.csp2_2 = CSPBlock(backbone_shape[3], backbone_shape[2], num_subblock=1, shortcut=False)

        self.cbl3 = ConvNormActivation(backbone_shape[2], backbone_shape[2],
                                       kernel_size=3, stride=2,
                                       activation=Swish)
        self.csp2_3 = CSPBlock(backbone_shape[3], backbone_shape[3], num_subblock=1, shortcut=False)

        self.cbl4 = ConvNormActivation(backbone_shape[3], backbone_shape[3],
                                       kernel_size=3, stride=2,
                                       activation=Swish)
        self.csp2_4 = CSPBlock(backbone_shape[4], backbone_shape[4], num_subblock=1, shortcut=False)

        self.backblock1 = nn.Conv2d(backbone_shape[2], out_channel, kernel_size=1, stride=1, has_bias=True)
        self.backblock2 = nn.Conv2d(backbone_shape[3], out_channel, kernel_size=1, stride=1, has_bias=True)
        self.backblock3 = nn.Conv2d(backbone_shape[4], out_channel, kernel_size=1, stride=1, has_bias=True)

        self.concat = ops.Concat(axis=1)

    def construct(self, x):
        """
        x is the input (x1, x2, x3)
        x1 is (batch_size, backbone_shape[2], h/8, w/8)
        x2 is (batch_size, backbone_shape[3], h/16, w/16)
        x3 is (batch_size, backbone_shape[4], h/32, w/32)
        """
        feature_map1, feature_map2, feature_map3 = x[0], x[1], x[2]

        # FPN
        cbl_output1 = self.cbl1(feature_map3)
        upsampling1 = ops.ResizeNearestNeighbor((38, 38))(cbl_output1)
        concat_output1 = self.concat((upsampling1, feature_map2))
        csp_output1 = self.csp2_1(concat_output1)

        cbl_output2 = self.cbl2(csp_output1)
        upsampling2 = ops.ResizeNearestNeighbor((76, 76))(cbl_output2)
        concat_output2 = self.concat((upsampling2, feature_map1))
        csp_output2 = self.csp2_2(concat_output2)

        # PAN
        cbl_output3 = self.cbl3(csp_output2)
        concat_output3 = self.concat((cbl_output3, cbl_output2))
        csp_output3 = self.csp2_3(concat_output3)

        cbl_output4 = self.cbl4(csp_output3)
        concat_output4 = self.concat((cbl_output4, cbl_output1))
        csp_output4 = self.csp2_4(concat_output4)

        small_object_output = self.backblock1(csp_output2)
        medium_object_output = self.backblock2(csp_output3)
        big_object_output = self.backblock3(csp_output4)

        return big_object_output, medium_object_output, small_object_output
