# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
""" Audio process. """

import librosa
import librosa.filters
import numpy as np
from scipy import signal
from scipy.io import wavfile


_fmax = 7600
_fmin = 55
_hop_size = 200
_max_abs_value = 4.
_min_level_db = -100
_n_fft = 800
_num_mels = 80
_preemphasis = 0.97
_ref_level_db = 20
_sample_rate = 16000
_win_size = 800
_mel_basis = None


def load_wav(path, sr):
    return librosa.core.load(path, sr=sr)[0]


def save_wav(wav, path, sr):
    wav *= 32767 / max(0.01, np.max(np.abs(wav)))
    wavfile.write(path, sr, wav.astype(np.int16))


def save_wavenet_wav(wav, path, sr):
    librosa.output.write_wav(path, wav, sr=sr)


def preemphasis(wav, k):
    return signal.lfilter([1, -k], [1], wav)


def linearspectrogram(wav):
    d = _stft(preemphasis(wav, _preemphasis))
    s = _amp_to_db(np.abs(d)) - _ref_level_db
    _normalize(s)
    return s


def melspectrogram(wav):
    d = _stft(preemphasis(wav, _preemphasis))
    s = _amp_to_db(_linear_to_mel(np.abs(d))) - _ref_level_db
    _normalize(s)
    return s


def _stft(y):
    return librosa.stft(y=y, n_fft=_n_fft, hop_length=_hop_size, win_length=_win_size)


def num_frames(length, fsize, fshift):
    """Compute number of time frames of spectrogram"""
    pad = (fsize - fshift)
    if length % fshift == 0:
        m = (length + pad * 2 - fsize) // fshift + 1
    else:
        m = (length + pad * 2 - fsize) // fshift + 2
    return m


def _linear_to_mel(spectogram):
    global _mel_basis
    if _mel_basis is None:
        _mel_basis = _build_mel_basis()
    return np.dot(_mel_basis, spectogram)


def _build_mel_basis():
    return librosa.filters.mel(_sample_rate, _n_fft, n_mels=_num_mels, fmin=_fmin, fmax=_fmax)


def _amp_to_db(x):
    min_level = np.exp(_min_level_db / 20 * np.log(10))
    return 20 * np.log10(np.maximum(min_level, x))


def _normalize(s):
    np.clip(_max_abs_value * ((s - _min_level_db) / (-_min_level_db)), 0, _max_abs_value)
    return _max_abs_value * ((s - _min_level_db) / (-_min_level_db))
