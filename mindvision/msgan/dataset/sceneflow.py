# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
""" Sceneflow dataset base class. """
from abc import abstractmethod
from PIL import Image

from mindvision.dataset.download import DownLoad
from mindvision.msgan.utils.sceneflow_utils import read_flow

__all__ = ["Sceneflow"]


class Sceneflow:
    """
    Sceneflow is the base class for sceneflow dataset. egs. Monkaa, flyingthings3d
    More dataset can be found at https://lmb.informatik.uni-freiburg.de/resources/datasets/SceneFlowDatasets.en.html
    """
    def __init__(self,
                 path: str,
                 download: bool = False):
        self.path = path
        if download:
            self.downloader = DownLoad()
            self.download_data()
        else:
            self.downloader = None
        self.data = self.load_data()
        self.trans = self.transforms()

    def __getitem__(self, index):
        entry = self.data[index]
        data = {
            'frame': Image.open(entry['frame']).convert("RGB"),
            'pre_frame': Image.open(entry['pre_frame']).convert("RGB"),
            'optical_flow': read_flow(entry['optical_flow']).copy(),
            'reverse_optical_flow': read_flow(entry['reverse_optical_flow']).copy(),
            'motion_boundaries': Image.open(entry['motion_boundaries'])
        }
        for operation in self.trans:
            data = operation(data)
        return (
            data['frame'],
            data['pre_frame'],
            data['optical_flow'],
            data['reverse_optical_flow'],
            data['motion_boundaries']
        )

    @abstractmethod
    def download_data(self):
        """Download dataset from url"""

    @abstractmethod
    def load_data(self):
        """Load dataset"""

    @abstractmethod
    def transforms(self):
        """Transform operation for dataset"""

    def __len__(self):
        return len(self.data)
