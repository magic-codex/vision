# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
""" Create the Monkaa dataset. """
import os
from mindvision.engine.class_factory import ClassFactory, ModuleType
from mindvision.msgan.dataset.sceneflow import Sceneflow
from mindvision.msgan.utils import custom_transforms
from mindvision.msgan.utils.sceneflow_utils import future_optical_flow_path, past_optical_flow_path,\
    motion_boundaries_path

__all__ = ["Monkaa"]


@ClassFactory.register(ModuleType.DATASET)
class Monkaa(Sceneflow):
    """
    A source dataset that downloads, reads, parses and augments the Monkaa dataset.

    The generated dataset has five columns :py:obj:`['frame', 'pre_frame', 'optical_flow', 'reverse_optical_flow',
    'motion_boundaries']`.
    The tensor of column :py:obj:`frame` is a matrix of the float32 type.
    The tensor of column :py:obj:`pre_frame` is a matrix of the float32 type.
    The tensor of column :py:obj:`optical_flow` is a matrix of the float32 type.
    The tensor of column :py:obj:`reverse_optical_flow` is a matrix of the float32 type.
    The tensor of column :py:obj:`motion_boundaries` is a matrix of the Bool type.

    Args:
        path (str): The root directory of the monkaa dataset.
        download (bool) : Whether to download the dataset. Default: False.

    Examples:
        >>> from mindvision.msgan.dataset.monkaa import Monkaa
        >>> import mindspore.dataset as ds
        >>> dataset_generator = Monkaa('./monkaa', download=False)
        >>> dataset = ds.GeneratorDataset(dataset_generator, ['frame', 'pre_frame', 'optical_flow',
        >>> 'reverse_optical_flow', 'motion_boundaries'])
        >>> for data in dataset.batch(batch_size=5):
        >>>     frame, pre_frame, optical_flow, reverse_optical_flow, motion_boundaries = data

    About Monkaa dataset:

    The collection contains more than 39000 stereo frames in 960x540 pixel resolution, rendered from various
    synthetic sequences. For details on the characteristics and differences of the three subsets, we refer the reader
    to our paper. The following kinds of data are currently available:

        Segmentations: Object-level and material-level segmentation images.

        Optical flow maps: The optical flow describes how pixels move between images (here, between time steps in a
        sequence). It is the projected screenspace component of full scene flow, and used in many computer vision
        applications.

        Disparity maps: Disparity here describes how pixels move between the two views of a stereo frame. It is a
        formulation of depth which is independent of camera intrinsics (although it depends on the configuration of
        the stereo rig), and can be seen as a special case of optical flow.

        Disparity change maps: Disparity alone is only valid for a single stereo frame. In image sequences,
        pixel disparities change with time. This disparity change data fills the gaps in scene flow that occur when
        one uses only optical flow and static disparity.

        Motion boundaries: Motion boundaries divide an image into regions with significantly different motion. They
        can be used to better judge the performance of an algorithm at discontinuities.

        Camera data: Full intrinsic and extrinsic camera data is available for each view of every stereo frame in our
        dataset collection.

    .. code-block::

        .
        └── Monkaa
             ├── frames_finalpass
             ├── motion_boundaries
             ├── optical_flow

    Citation:

    .. code-block::

        @InProceedings{MIFDB16, author    = "N. Mayer and E. Ilg and P. H{\"a}usser and P. Fischer and D. Cremers and
        A. Dosovitskiy and T. Brox", title     = "A Large Dataset to Train Convolutional Networks for Disparity,
        Optical Flow, and Scene Flow Estimation", booktitle = "IEEE International Conference on Computer Vision and
        Pattern Recognition (CVPR)", year      = "2016", note      = "arXiv:1512.02134", url       =
        "http://lmb.informatik.uni-freiburg.de/Publications/2016/MIFDB16" }
    """

    def load_data(self):
        """Load monkaa data from path"""
        data_list = []
        for dir_path, _, filenames in os.walk(os.path.join(self.path, "frames_finalpass")):
            if filenames == '':
                continue

            scene, side = dir_path.split(os.sep)[-2:]

            filenames.sort()
            filenames = [filename for filename in filenames if filename.endswith(".png")]

            for i in range(1, len(filenames)):
                frame_number = os.path.splitext(filenames[i])[0]
                pre_frame_number = os.path.splitext(filenames[i - 1])[0]
                data_list.append(
                    {
                        'frame': os.path.join(dir_path, filenames[i]),
                        'pre_frame': os.path.join(dir_path, filenames[i - 1]),
                        'optical_flow': future_optical_flow_path(self.path, scene, side, pre_frame_number),
                        'reverse_optical_flow': past_optical_flow_path(self.path, scene, side, frame_number),
                        'motion_boundaries': motion_boundaries_path(self.path, scene, side, frame_number)
                    }
                )
        return data_list

    def download_data(self):
        """Download monkaa dataset from url"""
        bool_list = []
        url_path = 'https://lmb.informatik.uni-freiburg.de/data/SceneFlowDatasets_CVPR16/Release_april16/data/Monkaa/'

        resources = [
            ("raw_data/monkaa__frames_finalpass.tar",
             "0d5417740421180ab6b9c435c9ca83ba"),
            ("derived_data/monkaa__motion_boundaries.tar.bz2",
             "31931073430b89adea474b0e30de0d97"),
            ("derived_data/monkaa__optical_flow.tar.bz2",
             "0db6f9d3fbe27275ba1eb97422f2bfb4")]
        # Check whether the file exists and check value of md5.
        for i in resources:
            filename, md5 = os.path.basename(i[0]), i[1]
            file_path = os.path.join(self.path, filename)
            bool_list.append(
                os.path.isfile(file_path) and self.download.check_md5(
                    file_path, md5)
            )

        if all(bool_list):
            return

        os.makedirs(self.path, exist_ok=True)

        # download files
        for filename, md5 in resources:
            url = os.path.join(url_path, filename)
            self.downloader.download_and_extract_archive(
                url, download_path=self.path, filename=os.path.basename(filename), md5=md5)

    def transforms(self):
        """Transform operations for monkaa"""
        return [
            custom_transforms.Resize(640, 360),
            custom_transforms.RandomHorizontalFlip(),
            custom_transforms.ToTensor()
        ]
