# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
""" Create the LRS2 dataset. """

import random
from glob import glob
import os
from os.path import dirname, join, basename, isfile
from typing import Optional, Callable, Union, Tuple
import cv2
import numpy as np
import mindspore.ops as ops
import mindspore as ms
from mindspore import Tensor
from mindvision.dataset.meta import Dataset, ParseDataset
from mindvision.check_param import Validator
from mindvision.engine.class_factory import ClassFactory, ModuleType
import utils.audio as audio

__all__ = ["LRS2SyncNet", "ParseLRS2"]


@ClassFactory.register(ModuleType.DATASET)
class LRS2SyncNet(Dataset):
    """
    A source dataset that reads, parses and augments the LRS2 dataset.
    The generated dataset has three columns :py:obj:`[image, wav, label]`.
    The tensor of column :py:obj:`image` is a matrix of the float32 type.
    The tensor of column :py:obj:`wav` is a matrix of the float32 type.
    The tensor of column :py:obj:`label` is a scalar of the float32 type.
    Args:
        path (str): The root directory of the LRS2 dataset or inference image.
        split (str): The dataset split, supports "train", "test" or "val". Default: "train".
        transform (callable, optional): A function transform that takes in a image. Default: None.
        target_transform (callable, optional): A function transform that takes in a label. Default: None.
        batch_size (int): The batch size of dataset. Default: 64.
        repeat_num (int): The repeat num of dataset. Default: 1.
        shuffle (bool, optional): Whether or not to perform shuffle on the dataset. Default: None.
        num_parallel_workers (int, optional): The number of subprocess used to fetch the dataset
            in parallel. Default: None.
        num_shards (int, optional): The number of shards that the dataset will be divided. Default: None.
        shard_id (int, optional): The shard ID within num_shards. Default: None.
        resize (Union[int, tuple]): The output size of the resized image. If size is an integer, the smaller edge of the
            image will be resized to this value with the same image aspect ratio. If size is a sequence of length 2,
            it should be (height, width). Default: 96.
    Examples:
        >>> from mindvision.classification.dataset import LRS2SyncNet
        >>> dataset = LRS2SyncNet("./data/LRS2", "train")
        >>> dataset = dataset.run()
    """

    def __init__(self,
                 path: str,
                 split: str = "train",
                 syncnet_t: int = 5,
                 syncnet_mel_step_size: int = 16,
                 transform: Optional[Callable] = None,
                 target_transform: Optional[Callable] = None,
                 batch_size: int = 64,
                 repeat_num: int = 1,
                 shuffle: Optional[bool] = None,
                 num_parallel_workers: int = 1,
                 num_shards: Optional[int] = None,
                 shard_id: Optional[int] = None,
                 resize: Union[int, Tuple[int, int]] = 96):
        Validator.check_string(split, ["train", "test", "val"], "split")
        mode = "L"
        columns_list = ["img", "wav", "label"]
        load_data = ParseLRS2(path=os.path.join(path, split),
                              split=split,
                              shard_id=shard_id,
                              syncnet_t=syncnet_t,
                              syncnet_mel_step_size=syncnet_mel_step_size,
                              resize=resize)
        super(LRS2SyncNet, self).__init__(path=path,
                                          split=split,
                                          load_data=load_data,
                                          transform=transform,
                                          target_transform=target_transform,
                                          batch_size=batch_size,
                                          repeat_num=repeat_num,
                                          resize=resize,
                                          shuffle=shuffle,
                                          num_parallel_workers=num_parallel_workers,
                                          num_shards=num_shards,
                                          shard_id=shard_id,
                                          mode=mode,
                                          columns_list=columns_list)

    @property
    def index2label(self):
        return

    def default_transform(self):
        return


class ParseLRS2(ParseDataset):
    """
    Parse LRS2 dataset.

    Args:
        path (str): The root path of the LRS2 dataset join train or test.

    Examples:
        >>> parse_data = ParseLRS2("./LRS2/train")
    """

    def __init__(self,
                 path: str,
                 split: str = "train",
                 shard_id: Optional[int] = None,
                 syncnet_t: int = 5,
                 syncnet_mel_step_size: int = 16,
                 resize: Union[int, Tuple[int, int]] = 96,
                 fps: int = 25,
                 sample_rate: int = 16000):
        super(ParseLRS2, self).__init__(path, shard_id)
        self.all_videos = self._get_image_list(path, split)
        self.syncnet_t = syncnet_t
        self.syncnet_mel_step_size = syncnet_mel_step_size
        self.resize = resize
        self.fps = fps
        self.sample_rate = sample_rate

    def _get_image_list(self, path, split):
        filelist = []
        with open('filelists/{}.txt'.format(split)) as f:
            for line in f:
                line = line.strip()
                if ' ' in line:
                    line = line.split()[0]
                filelist.append(os.path.join(path, line))
        return filelist

    def get_frame_id(self, frame):
        return int(basename(frame).split('.')[0])

    def get_window(self, start_frame):
        start_id = self.get_frame_id(start_frame)
        vidname = dirname(start_frame)
        window_fnames = []
        for frame_id in range(start_id, start_id + self.syncnet_t):
            frame = join(vidname, '{}.jpg'.format(frame_id))
            if not isfile(frame):
                return None
            window_fnames.append(frame)
        return window_fnames

    def crop_audio_window(self, spec, start_frame):
        start_frame_num = self.get_frame_id(start_frame)
        start_idx = int(80. * (start_frame_num / float(self.fps)))
        end_idx = start_idx + self.syncnet_mel_step_size
        return spec[start_idx: end_idx, :]

    def __load_data(self, idx):
        """Parse data from LRS2 dataset file."""
        while 1:
            idx = random.randint(0, len(self.all_videos) - 1)
            vidname = self.all_videos[idx]

            img_names = list(glob(join(vidname, '*.jpg')))
            if len(img_names) <= 3 * self.syncnet_t:
                continue
            img_name = random.choice(img_names)
            wrong_img_name = random.choice(img_names)
            while wrong_img_name == img_name:
                wrong_img_name = random.choice(img_names)

            if random.choice([True, False]):
                label = ops.Ones()(1, ms.float32)
                chosen = img_name
            else:
                label = ops.Zeros()(1, ms.float32)
                chosen = wrong_img_name

            window_fnames = self.get_window(chosen)
            if window_fnames is None:
                continue

            window = []
            all_read = True
            for fname in window_fnames:
                img = cv2.imread(fname)
                if img is None:
                    all_read = False
                    break
                img = cv2.resize(img, (self.resize, self.resize))
                window.append(img)

            if not all_read:
                continue

            wav_path = join(vidname, "audio.wav")
            wav = audio.load_wav(wav_path, self.sample_rate)
            orig_mel = audio.melspectrogram(wav).T
            mel = self.crop_audio_window(orig_mel.copy(), img_name)

            image = np.concatenate(window, axis=2) / 255.
            image = image.transpose(2, 0, 1)
            image = image[:, image.shape[1] // 2:]
            image = Tensor(image, ms.float32)
            mel = ops.ExpandDims()(Tensor(mel.T, ms.float32), 0)
            return image, mel, label

    def parse_dataset(self, *args):
        """Parse dataset."""
        if not args:
            return [self.all_videos]
        return self.__load_data(args[0])
