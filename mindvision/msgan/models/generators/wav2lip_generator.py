# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Wav2Lip."""

from typing import List
import mindspore.nn as nn
import mindspore.ops as ops
from mindvision.classification.models.blocks import Conv2dNormResAct, Conv2dTransPadBN

__all__ = [
    "Wav2Lip",
]


class Wav2Lip(nn.Cell):
    """
    Wav2Lip definition.

    Args:
        face_enc_in_chan (int): The input channel of face encoder.
        face_enc_out_chan (int): The output channel offace encoder.
        face_enc_layer_nums (int): The numbers of block in different layers.
        audio_in_chan (int): The input channel of audio encoder.
        audio_out_chan (int): The output channel of audio encoder.
        face_dec_in_chans (list): The input channel list of face decoder.
        face_dec_out_chans (list): The output channel list of face decoder.

    Returns:
        Tensor, output tensor.

    Examples:
        >>> from mindvision.msgan.models.generators import Wav2Lip
        >>> Wav2Lip(6, 16, [2, 3, 2, 2, 1], 1, 32, [1024, 768, 512, 320, 160], [512, 384, 258, 128, 64])
    """

    def __init__(self,
                 face_enc_in_chan,
                 face_enc_out_chan,
                 face_enc_layer_nums: List[int],
                 audio_in_chan,
                 audio_out_chan,
                 face_dec_in_chans: List[int],
                 face_dec_out_chans: List[int],
                 ):
        super(Wav2Lip, self).__init__()

        self.face_encode_blocks = nn.CellList(
            [Conv2dNormResAct(face_enc_in_chan, face_enc_out_chan, kernel_size=7, stride=1, padding=3)]
        )
        for i in range(len(face_enc_layer_nums)):
            self.face_encode_blocks.extend([self._make_face_en(face_enc_layer_nums[i], face_enc_out_chan * (2 ** i))])
        face_enc_out_chan = face_enc_out_chan * (2 ** len(face_enc_layer_nums))
        self.face_encode_blocks.extend([
            Conv2dNormResAct(face_enc_out_chan, face_enc_out_chan, kernel_size=3, stride=1, padding=0),  # 1, 1
            Conv2dNormResAct(face_enc_out_chan, face_enc_out_chan, kernel_size=1, stride=1, padding=0),
        ])

        self.audio_encoder = nn.SequentialCell()
        self.audio_encoder.append(self._make_audio_en(audio_in_chan, audio_out_chan))
        self.audio_encoder.append(self._make_audio_en(audio_out_chan * 2, audio_out_chan * 4, (3, 1)))
        self.audio_encoder.append(self._make_audio_en(audio_out_chan * 4, audio_out_chan * 8, 3))
        self.audio_encoder.append(self._make_audio_en(audio_out_chan * 8, audio_out_chan * 16, (3, 2)))
        self.audio_encoder.append(
            Conv2dNormResAct(256, 512, kernel_size=3, stride=1, padding=0),
            Conv2dNormResAct(512, 512, kernel_size=1, stride=1, padding=0),
        )

        self.face_decoder_blocks = nn.CellList([Conv2dNormResAct(512, 512, kernel_size=1, stride=1, padding=0),
                                                Conv2dTransPadBN(1024, 512, kernel_size=3, stride=1, padding=0),  # 3,3
                                                Conv2dNormResAct(512, 512, kernel_size=3, stride=1,
                                                                 padding=1, residual=True)])
        for i in range(len(face_dec_in_chans)):
            self.face_decoder_blocks.extend([Conv2dTransPadBN(face_dec_in_chans[i], face_dec_out_chans[i], 3, 2, 1, 1)])
            self.face_decoder_blocks.extend(
                [Conv2dNormResAct(face_dec_out_chans[i], face_dec_out_chans[i], 3, 1, 1, True)] * 2
            )

        self.output_block = nn.SequentialCell(
            Conv2dNormResAct(80, 32, kernel_size=3, stride=1, padding=1),
            nn.Conv2d(32, 3, kernel_size=1, stride=1, pad_mode="pad", padding=0, has_bias=True),
            nn.Sigmoid()
        )

    def _make_face_en(self, nums, channels):
        out = [Conv2dNormResAct(channels, channels * 2, kernel_size=3, stride=2, padding=1)]
        channels = channels * 2
        out += [Conv2dNormResAct(channels, channels, kernel_size=3, stride=1, padding=1, residual=True)] * nums
        return out

    def _make_audio_en(self, in_channel, out_channel, stride=1):
        if in_channel != 1:
            out_channel = in_channel * 2
        out = [Conv2dNormResAct(in_channel, out_channel, kernel_size=3, stride=stride, padding=1)]
        copy_nums = 2
        if out_channel == 256:
            copy_nums = 1
        out += [Conv2dNormResAct(out_channel, out_channel, kernel_size=3,
                                 stride=1, padding=1, residual=True)] * copy_nums
        return out

    def construct(self, audio_sequences, face_sequences):
        """Wav2Lip construct"""
        b = audio_sequences.shape[0]

        input_dim_size = len(face_sequences.shape)
        if input_dim_size > 4:
            audio_sequences = ops.Concat(0)([audio_sequences[:, i] for i in range(audio_sequences.shape[1])])
            face_sequences = ops.Concat(0)([face_sequences[:, :, i] for i in range(face_sequences.shape[2])])

        audio_embedding = self.audio_encoder(audio_sequences)  # B, 512, 1, 1
        feats = []
        x = face_sequences
        for f in self.face_encoder_blocks:
            x = f(x)
            feats.append(x)

        x = audio_embedding
        for f in self.face_decoder_blocks:
            x = f(x)
            x = ops.Concat(1)((x, feats[-1]))
            feats.pop()

        x = self.output_block(x)

        if input_dim_size > 4:
            x = ops.Split(0, b)(x)  # [(B, C, H, W)]
            outputs = ops.Stack(2)(x)  # (B, C, T, H, W)
        else:
            outputs = x
        outputs = ops.stop_gradient(outputs)
        return outputs
