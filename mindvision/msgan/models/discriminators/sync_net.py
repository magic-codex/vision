# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""SyncNet."""

from typing import List
import mindspore.nn as nn
import mindspore.ops as ops

from mindvision.classification.models.blocks import Conv2dNormResAct
from mindvision.engine.class_factory import ClassFactory, ModuleType

__all__ = [
    "FaceEncoder",
    "AudioEncoder",
    "SyncNet"
]


class FaceEncoder(nn.Cell):
    """
    FaceEncoder block definition.

    Args:
        layer_nums (list): The numbers of block in different layers.

    Returns:
        Tensor, output tensor.

    Examples:
        >>> from mindvision.msgan.models.discriminators import FaceEncoder
        >>> FaceEncoder([2, 3, 2, 2])
    """

    def __init__(self, layer_nums: List[int]):
        super(FaceEncoder, self).__init__()
        self.net = nn.SequentialCell()
        self.net.append([
            Conv2dNormResAct(15, 32, kernel_size=7, stride=1, padding=3),
        ])
        self.net.append(self._make_lay(layer_nums[0], 32, 5, (1, 2)))
        self.net.append(self._make_lay(layer_nums[1], 64))
        self.net.append(self._make_lay(layer_nums[2], 128))
        self.net.append(self._make_lay(layer_nums[3], 256))
        self.net.append(Conv2dNormResAct(512, 512, kernel_size=3, stride=2, padding=1),
                        Conv2dNormResAct(512, 512, kernel_size=3, stride=1, padding=0),
                        Conv2dNormResAct(512, 512, kernel_size=1, stride=1, padding=0))

    def _make_lay(self, nums, channels, kernel_size=3, stride=1):
        out = (Conv2dNormResAct(channels, channels * 2, kernel_size, stride, padding=1),)
        channels = channels * 2
        out += (Conv2dNormResAct(channels, channels, kernel_size=3, stride=1, padding=1, residual=True),) * nums
        return out

    def construct(self, face_sequences):
        return self.net(face_sequences)


class AudioEncoder(nn.Cell):
    """
    AudioEncoder block definition.

    Args:
        in_channel (int): The first Conv2d layer's output channel.

    Returns:
        Tensor, output tensor.

    Examples:
        >>> from mindvision.msgan.models.discriminators import AudioEncoder
        >>> AudioEncoder(32)
    """

    def __init__(self, in_channel=32):
        super(AudioEncoder, self).__init__(self)
        self.in_channel = in_channel
        self.net = nn.SequentialCell()
        self.net.append(self._make_lay(1))
        self.net.append(self._make_lay(self.in_channel, (3, 1)))
        self.net.append(self._make_lay(self.in_channel * 2, 3))
        self.net.append(self._make_lay(self.in_channel * 4, (3, 2)))
        self.net.append([
            Conv2dNormResAct(in_channel * 8, in_channel * 16, kernel_size=3, stride=1, padding=0),
            Conv2dNormResAct(in_channel * 16, in_channel * 16, kernel_size=1, stride=1, padding=0)
        ])

    def _make_lay(self, channels, stride=1):
        out_channel = self.in_channel
        if channels != 1:
            out_channel = channels * 2
        out = (Conv2dNormResAct(channels, out_channel, kernel_size=3, stride=stride, padding=1),)
        out += (Conv2dNormResAct(out_channel, out_channel, kernel_size=3, stride=1, padding=1, residual=True),) * 2
        return out

    def construct(self, audio_sequences):
        return self.net(audio_sequences)


@ ClassFactory.register(ModuleType.GENERATOR)
class SyncNet(nn.Cell):
    """
    SyncNet architecture.

    Args:
        face_encoder (nn.Cell): face_encoder, Default: FaceEncoder([2, 3, 2, 2]).
        audio_encoder (nn.Cell): audio_encoder, Default: AudioEncoder(32).
        norm (nn.Cell): The module specifying the normalization layer to use. Default: ops.L2Normalize(1).

    Inputs:
        - **audio_sequences** (Tensor) - Tensor of shape :math:`(N, C_{in}, H_{in}, W_{in})`.
        - **face_sequences** (Tensor) - Tensor of shape :math:`(N, C_{in}, H_{in}, W_{in})`.

    Outputs:
        Tensor of shape : (N, 512), (N, 512)

    Supported Platforms:
        ``CPU`` ``GPU`` ``Ascend``

    Examples:
        >>> import numpy as np
        >>> import mindspore as ms
        >>> from mindvision.msgan.models.discriminators import SyncNet, FaceEncoder, AudioEncoder
        >>> net = SyncNet(FaceEncoder([2, 3, 2, 2]), AudioEncoder(32), ops.L2Normalize(1))
        >>> audio = ms.Tensor(np.ones([64, 1, 80, 16]), ms.float32)
        >>> face = ms.Tensor(np.ones([64, 15, 48, 96]), ms.float32)
        >>> output_au, output_fa = net(audio, face)
        >>> print(output_au.shape, output_fa.shape)
        (64, 512) (64, 512)
    """

    def __init__(self,
                 face_encoder: nn.Cell = FaceEncoder([2, 3, 2, 2]),
                 audio_encoder: nn.Cell = AudioEncoder(32),
                 normal=ops.L2Normalize(1)):
        super(SyncNet, self).__init__()
        self.face_encoder = face_encoder
        self.audio_encoder = audio_encoder
        self.normal = normal

    def construct(self, audio_sequences, face_sequences):
        """SyncNet construct"""
        face_embedding = self.face_encoder(face_sequences)
        audio_embedding = self.audio_encoder(audio_sequences)

        audio_embedding = audio_embedding.view(audio_embedding.shape[0], -1)
        face_embedding = face_embedding.view(face_embedding.shape[0], -1)

        audio_embedding = self.normal(audio_embedding)
        face_embedding = self.normal(face_embedding)

        return audio_embedding, face_embedding
