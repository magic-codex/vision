# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""DCGAN models."""

import numpy as np
from mindspore import dtype as mstype
from mindspore import nn, Tensor
from mindspore.common.initializer import Normal

from mindvision.msgan.losses.loss import GeneratorLoss, DiscriminatorLoss
from .base import Discriminator, Generator

np.random.seed(1)
__all__ = ["DCGANGenerator", "DCGANDiscriminator", "DCGAN"]


class DCGANGenerator(Generator):
    """ Deep Convolutional GAN (DCGAN) generator from
    `"Unsupervised Representation Learning With Deep Convolutional Generative Aversarial Networks
    by Radford et. al. "<https://arxiv.org/abs/1511.06434>` paper.
    Args:
        encoding_dims(int): the dimensions of vectors. Default: 100.
        out_size (int): Output size for the generated image.
         The tuple of two integers will set the height and width
            for the output image, respectively. Default: 32.
        out_channels (int): The channel number of the output feature.
            Default: 3.
        step_channels (int): The basic channel number of the
            generator. The other layers contains channels based on this number.
            Default: 64.
        weight_init (tensor): the initiated weight of Conv2d.
            Default: normal distribution(mean=0, sigma=0.02).
        gamma_init(tensor): the initiated optimizer parameters of BatchNorm2d.
            Default: normal distribution(mean=0, sigma=0.02).
        label_type(str): the type of label. Default: "none".
    """

    def __init__(self,
                 encoding_dims=100,
                 out_size=32,
                 out_channels=3,
                 step_channels=64,
                 label_type="none",
                 weight_init=Normal(mean=0, sigma=0.02),
                 gamma_init=Normal(mean=1, sigma=0.02)
                 ):
        super(DCGANGenerator, self).__init__(encoding_dims, label_type)
        self.out_size = out_size
        self.out_channels = out_channels
        self.step_channels = step_channels
        self.encoding_dims = encoding_dims
        self.generator = nn.SequentialCell()
        self.generator.append(nn.Conv2dTranspose(self.encoding_dims, self.step_channels * 8, 4, 1,
                                                 pad_mode="pad", padding=0, weight_init=weight_init))
        self.generator.append(nn.BatchNorm2d(self.step_channels * 8, gamma_init=gamma_init))
        self.generator.append(nn.ReLU())
        self.generator.append(nn.Conv2dTranspose(self.step_channels * 8, self.step_channels * 4, 4, 2,
                                                 pad_mode="pad", padding=1, weight_init=weight_init))
        self.generator.append(nn.BatchNorm2d(self.step_channels * 4, gamma_init=gamma_init))
        self.generator.append(nn.ReLU())
        self.generator.append(nn.Conv2dTranspose(self.step_channels * 4, self.step_channels * 2, 4, 2,
                                                 pad_mode="pad", padding=1, weight_init=weight_init))
        self.generator.append(nn.BatchNorm2d(self.step_channels * 2, gamma_init=gamma_init))
        self.generator.append(nn.ReLU())
        self.generator.append(nn.Conv2dTranspose(self.step_channels * 2, self.step_channels, 4, 2,
                                                 pad_mode="pad", padding=1, weight_init=weight_init))
        self.generator.append(nn.BatchNorm2d(self.step_channels, gamma_init=gamma_init))
        self.generator.append(nn.ReLU())
        self.generator.append(nn.Conv2dTranspose(self.step_channels, self.out_channels, 4, 2,
                                                 pad_mode="pad", padding=1, weight_init=weight_init))
        self.generator.append(nn.Tanh())

    def construct(self, x):
        """generator construct"""
        return self.generator(x)


class DCGANDiscriminator(Discriminator):
    """Deep Convolutional GAN (DCGAN) discriminator from
    `"Unsupervised Representation Learning With Deep Convolutional Generative Aversarial Networks
    by Radford et. al. "<https://arxiv.org/abs/1511.06434>`.
    Args:
        in_size(int): Output size for the generated
            image. The tuple of two integers will set the height and width
            for the output image, respectively. Default: 32.
        in_channels(int): The channel number of the output feature.
            Default: 3.
        step_channels(int): The basic channel number of the
            generator. The other layers contains channels based on this number.
            Default: 64.
        weight_init(tensor): the initiated weight of Conv2d.
            Default: normal distribution(mean=0, sigma=0.02).
        gamma_init(tensor): the initiated optimizer parameters of BatchNorm2d.
            Default: normal distribution(mean=0, sigma=0.02).
        label_type(str): the type of label. Default: "none".
    """

    def __init__(self,
                 in_size=32,
                 in_channels=3,
                 step_channels=64,
                 label_type="none",
                 weight_init=Normal(mean=0, sigma=0.02),
                 gamma_init=Normal(mean=1, sigma=0.02)):
        super(DCGANDiscriminator, self).__init__(in_channels, label_type)
        self.in_size = in_size
        self.step_channels = step_channels
        self.discriminator = nn.SequentialCell()
        self.discriminator.append(nn.Conv2d(in_channels, step_channels, 4, 2, pad_mode="pad",
                                            padding=1, weight_init=weight_init))
        self.discriminator.append(nn.LeakyReLU(0.2))
        self.discriminator.append(nn.Conv2d(step_channels, step_channels * 2, 4, 2, pad_mode="pad",
                                            padding=1, weight_init=weight_init))
        self.discriminator.append(nn.BatchNorm2d(step_channels * 2, gamma_init=gamma_init))
        self.discriminator.append(nn.LeakyReLU(0.2))
        self.discriminator.append(nn.Conv2d(step_channels * 2, step_channels * 4, 4, 2, pad_mode="pad",
                                            padding=1, weight_init=weight_init))
        self.discriminator.append(nn.BatchNorm2d(step_channels * 4, gamma_init=gamma_init))
        self.discriminator.append(nn.LeakyReLU(0.2))
        self.discriminator.append(nn.Conv2d(step_channels * 4, step_channels * 8, 4, 2,
                                            pad_mode="pad", padding=1, weight_init=weight_init))
        self.discriminator.append(nn.BatchNorm2d(step_channels * 8, gamma_init=gamma_init))
        self.discriminator.append(nn.LeakyReLU(0.2))
        self.discriminator.append(nn.Conv2d(step_channels * 8, 1, 4,
                                            pad_mode="pad", padding=1, weight_init=weight_init))
        self.discriminator.append(nn.Sigmoid())

    def construct(self, x):
        """discriminator construct"""
        return self.discriminator(x)


class DCGAN(nn.Cell):
    """
    define DCGAN
    Args:
        lr(double): Learning rate of optimizer. Default: 0.02.
        beta1(double): Beta parameter of  adam optimizer. Default: 0.5.
        in_channels(int): The channel number of input picture. Default: 3.
     """

    def __init__(self, lr=0.02, beta1=0.5, in_channels=3):
        super(DCGAN, self).__init__(auto_prefix=True)
        self.generator = DCGANGenerator()
        # instantiation of generator
        self.discriminator = DCGANDiscriminator()
        self.criterion = nn.BCELoss(reduction='mean')
        self.discriminator_with_criterion = DiscriminatorLoss(self.discriminator, self.generator, self.criterion)
        self.generator_with_criterion = GeneratorLoss(self.discriminator, self.generator, self.criterion)
        self.fixed_noise = Tensor(np.random.randn(64, in_channels, 1, 1), dtype=mstype.float32)
        # set optimizer of generator and discriminator
        self.optimizer_discriminator = nn.Adam(self.discriminator.trainable_params(), learning_rate=lr, beta1=beta1)
        self.optimizer_generator = nn.Adam(self.generator.trainable_params(), learning_rate=lr, beta1=beta1)
        # instantiation of TrainOneStepCell
        self.train_step_cell_discriminator = nn.TrainOneStepCell(self.discriminator_with_criterion,
                                                                 self.optimizer_discriminator)
        self.train_step_cell_generator = nn.TrainOneStepCell(self.generator_with_criterion, self.optimizer_generator)
        self.g_losses = []
        self.d_losses = []
        self.image_list = []

    def construct(self, real_data, latent_code):
        """DCGAN construct"""
        output_d = self.train_step_cell_discriminator(real_data, latent_code).view(-1)
        discriminator_loss = output_d.mean()
        output_g = self.train_step_cell_generator(latent_code).view(-1)
        generator_loss = output_g.mean()
        return discriminator_loss, generator_loss
