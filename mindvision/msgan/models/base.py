# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""GAN base for Generator and Discriminator."""

from mindspore import nn


__all__ = ["Generator", "Discriminator"]


class Generator(nn.Cell):
    """TODO. Base class for all Generator models.
    """

    def __init__(self, encoding_dims, label_type="none"):
        super(Generator, self).__init__()
        self.encoding_dims = encoding_dims
        self.label_type = label_type

class Discriminator(nn.Cell):
    """TODO. Base class for all Discriminator models.
    """

    def __init__(self, input_dims, label_type="none"):
        super(Discriminator, self).__init__()
        self.input_dims = input_dims
        self.label_type = label_type
