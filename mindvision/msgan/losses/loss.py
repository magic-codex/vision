# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Base class for all generator or discriminator losses."""

from mindspore import nn, ops
__all__ = ["GeneratorLoss", "DiscriminatorLoss"]


class GeneratorLoss(nn.Cell):
    """
    All Losses meant to be minimized for optimizing the Generator must subclass this.
    Args:
        discriminator_net(nn.Cell): The discriminator network.
        generator_net(nn.Cell): The generator network.
        loss_fn(nn.LossBase): The loss function.
    """
    def __init__(self, discriminator_net, generator_net, loss_fn):
        super(GeneratorLoss, self).__init__(auto_prefix=True)
        self.discriminator_net = discriminator_net
        self.generator_net = generator_net
        self.loss_fn = loss_fn

    def construct(self, latent_code):
        """construct generator structure"""
        fake_data = self.generator_net(latent_code)
        out = self.discriminator_net(fake_data)
        label_real = ops.OnesLike()(out)
        loss = self.loss_fn(out, label_real)
        return loss


class DiscriminatorLoss(nn.Cell):
    """
    Base class for all discriminator losses.
    All Losses meant to be minimized for optimizing the Discriminator must subclass this.
    Args:
        discriminator_net(nn.Cell): discriminator network.
        generator_net(nn.Cell): generator network.
        loss_fn(nn.LossBase): the loss function.
    """
    def __init__(self, discriminator_net, generator_net, loss_fn):
        super(DiscriminatorLoss, self).__init__(auto_prefix=True)
        self.discriminator_net = discriminator_net
        self.generator_net = generator_net
        self.loss_fn = loss_fn

    def construct(self, real_data, latent_code):
        """construct discriminator structure"""
        out_real = self.discriminator_net(real_data)
        label_real = ops.OnesLike()(out_real)
        loss_real = self.loss_fn(out_real, label_real)

        fake_data = self.generator_net(latent_code)
        fake_data = ops.stop_gradient(fake_data)
        out_fake = self.discriminator_net(fake_data)
        label_fake = ops.ZerosLike()(out_fake)
        loss_fake = self.loss_fn(out_fake, label_fake)
        return loss_real + loss_fake
