# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Pooling and layer norm neck."""

import mindspore.nn as nn
from mindspore.ops import operations as P

from mindvision.engine.class_factory import ClassFactory, ModuleType


@ClassFactory.register(ModuleType.NECK)
class AvgPoolingLayerNorm(nn.Cell):
    """
    Global avg pooling and layer norm definition.

    Args:
        num_channels (int): The channels num of layer normalization.

    Returns:
        Tensor, output tensor.

    Examples:
        >>> AvgPoolingLayerNorm(num_channels=256)
    """

    def __init__(self,
                 num_channels,
                 ) -> None:
        super(AvgPoolingLayerNorm, self).__init__()
        self.mean = P.ReduceMean()
        self.layer_norm = nn.LayerNorm(normalized_shape=(num_channels,),
                                       begin_norm_axis=1,
                                       begin_params_axis=1)

    def construct(self, x):
        x = self.mean(x, (2, 3))
        x = self.layer_norm(x)
        return x
