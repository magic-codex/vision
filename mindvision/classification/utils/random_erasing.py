import math
import random

import numpy as np


# 生成对应格式和size的数据
def _get_pixels(per_pixel, rand_color, patch_size, dtype=np.float32):
    """_get_pixels"""
    if per_pixel:
        func = np.random.normal(size=patch_size).astype(dtype)
    elif rand_color:
        func = np.random.normal(size=(patch_size[0], 1, 1)).astype(dtype)
    else:
        func = np.zeros((patch_size[0], 1, 1), dtype=dtype)
    return func


class RandomErasing:
    """ 随机选择图像中的一个矩形区域并擦除其像素。
    
        这个随机擦除的变体旨在应用于一个批次的或单个图像张量，在它被数据集的平均值和标准值规范化之后。
    Args:
         概率：执行随机擦除操作的概率。
         min_area：擦除区域相对于输入图像区域的最小百分比。
         max_area：擦除区域相对于输入图像区域的最大百分比。
         min_aspect：擦除区域的最小纵横比。
         mode：像素颜色模式，"const"、"rand "或 "pixel "之一。
            const'--擦除块的所有通道的颜色都是恒定的，即0。
            'rand' - 擦除块是每个通道的随机（正常）颜色
            pixel' - 擦除块是每个像素的随机（正常）颜色
        max_count：每幅图像的最大擦除块数，每个盒子的面积按计数缩放。
            每幅图像的计数是在1和这个值之间随机选择的。
    """

    # 初始化，设置对应的参数数值
    def __init__(self, probability=0.5, min_area=0.02, max_area=1 / 3, min_aspect=0.3,
                 max_aspect=None, mode='const', min_count=1, max_count=None, num_splits=0):
        self.probability = probability
        self.min_area = min_area
        self.max_area = max_area
        max_aspect = max_aspect or 1 / min_aspect
        self.log_aspect_ratio = (math.log(min_aspect), math.log(max_aspect))
        self.min_count = min_count
        self.max_count = max_count or min_count
        self.num_splits = num_splits
        mode = mode.lower()
        self.rand_color = False
        self.per_pixel = False
        if mode == 'rand':
            self.rand_color = True  # per block random normal
        elif mode == 'pixel':
            self.per_pixel = True  # per pixel random normal
        else:
            assert not mode or mode == 'const'

    # 擦除操作
    def _erase(self, img, chan, img_h, img_w, dtype):

        # 设置对应的随机数，与擦除可能性阈值进行比较
        if random.random() > self.probability:
            pass
        else:
            # 计算面积
            area = img_h * img_w
            # 设置要磨除的个数，在最小和最大之间
            count = self.min_count if self.min_count == self.max_count else \
                random.randint(self.min_count, self.max_count)
            for _ in range(count):
                for _ in range(10):
                    # 计算最终呗mask的面积
                    target_area = random.uniform(self.min_area, self.max_area) * area / count
                    # 计算mask的占比
                    aspect_ratio = math.exp(random.uniform(*self.log_aspect_ratio))
                    # 计算被mask的宽和高
                    h = int(round(math.sqrt(target_area * aspect_ratio)))
                    w = int(round(math.sqrt(target_area / aspect_ratio)))
                    # 判断是否符合要求
                    if w < img_w and h < img_h:
                        top = random.randint(0, img_h - h)
                        left = random.randint(0, img_w - w)
                        img[:, top:top + h, left:left + w] = _get_pixels(
                            self.per_pixel, self.rand_color, (chan, h, w),
                            dtype=dtype)
                        break
        # 返回图片
        return img

    def __call__(self, x):
        # 执行随机mask

        # 如果格式符合直接调用方法
        if len(x.shape) == 3:
            output = self._erase(x, *x.shape, x.dtype)
        # 如果格式不符合，调整为正确格式之后在执行方法
        else:
            output = np.zeros_like(x)
            batch_size, chan, img_h, img_w = x.shape
            # skip first slice of batch if num_splits is set (for clean portion of samples)
            batch_start = batch_size // self.num_splits if self.num_splits > 1 else 0
            for i in range(batch_start, batch_size):
                output[i] = self._erase(x[i], chan, img_h, img_w, x.dtype)
        return output
