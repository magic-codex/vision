# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
""" The public API for video dataset. """

import os
import mindspore.dataset as ds
from mindvision.dataset.meta import Dataset
from mindvision.msvideo.dataset import transforms
from mindvision.msvideo.dataset.generator import DatasetGenerator


class VideoDataset(Dataset):
    """
    VideoDataset is the base class for making video dataset which are compatible with MindSpore Vision.
    """

    def __init__(self,
                 path,
                 split,
                 load_data,
                 transform,
                 target_transform,
                 seq,
                 seq_mode,
                 align,
                 batch_size,
                 repeat_num,
                 shuffle,
                 num_parallel_workers,
                 num_shards,
                 shard_id,
                 download,
                 columns_list=('video', 'label'),
                 suffix="video"
                 ):
        if columns_list[0] == 'image':
            super(VideoDataset, self).__init__(path=path,
                                               split=split,
                                               load_data=load_data,
                                               transform=transform,
                                               target_transform=target_transform,
                                               batch_size=batch_size,
                                               repeat_num=repeat_num,
                                               resize=None,
                                               shuffle=shuffle,
                                               num_parallel_workers=num_parallel_workers,
                                               num_shards=num_shards,
                                               shard_id=shard_id,
                                               download=download)
        ds.config.set_enable_shared_mem(False)
        self.path = os.path.expanduser(path)
        self.split = split
        self.download = download

        if self.download:
            self.download_dataset()
        self.transform = transform
        self.video_path, self.label = load_data()
        self.target_transform = target_transform
        self.seq = seq
        self.seq_mode = seq_mode
        self.align = align
        self.batch_size = batch_size
        self.repeat_num = repeat_num
        self.shuffle = shuffle
        self.num_parallel_workers = num_parallel_workers
        self.num_shards = num_shards
        self.shard_id = shard_id
        self.columns_list = columns_list
        self.suffix = suffix
        self.dataset = ds.GeneratorDataset(DatasetGenerator(path=self.video_path,
                                                            label=self.label,
                                                            seq=self.seq,
                                                            mode=self.seq_mode,
                                                            suffix=self.suffix,
                                                            align=self.align),
                                           column_names=['video', 'label'],
                                           num_parallel_workers=num_parallel_workers,
                                           shuffle=self.shuffle,
                                           num_shards=self.num_shards,
                                           shard_id=self.shard_id)

    def download_dataset(self):
        """Download the dataset."""
        raise NotImplementedError

    @property
    def index2label(self):
        """Get the mapping of indexes and labels."""
        raise NotImplementedError

    def default_transform(self):
        """Set the default transform for video dataset."""
        size = (224, 224)
        order = (3, 0, 1, 2)
        trans = [
            transforms.VideoResize(size),
            transforms.VideoReOrder(order),
        ]

        return trans
