# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
""" Video Dataset Generator. """

import random
import imageio.v3 as iio
import numpy as np


class DatasetGenerator:
    """ Dataset generator for getting video path and its corresponding label. """

    def __init__(self, path, label, seq=16, mode="part", suffix="video", align=False):
        """
        Init Video Generator.
        Args:
           path(list): Video file path list.
           label(list): The label of each video,
           seq(int): The number of frames of the intercepted video.
           mode(str): Frame fetching method.
           suffix(str): Format of video file.
           align(boolean): The video contains multiple actions.

        """
        self.path = path
        self.label = label
        self.seq = seq
        self.mode = mode
        self.suffix = suffix
        self.align = align

    def __getitem__(self, item):
        """Get the video and label for each item."""

        if self.suffix == "picture":
            frame_path_list = self.path[item]
            video = []
            for frame_path in frame_path_list:
                with open(frame_path, "rb")as f:
                    content = f.read()
                frame = iio.imread(content, index=None, format_hint=".jpg")
                video.append(frame)
            video = np.concatenate(video, axis=0)
        if self.suffix == "video":
            with open(self.path[item], 'rb')as rf:
                content = rf.read()
            video = iio.imread(content, index=None, format_hint=".avi")
        label = self.label[item]
        if self.align:
            pos_start = self.label[item][1]
            pos_end = self.label[item][2]
            frame_len = video.shape[0]
            video = video[int(pos_start * frame_len):int(pos_end * frame_len)]
            label = self.label[item][0]
        if self.mode == "part":
            start = random.randint(0, video.shape[0] - self.seq)
            video = video[start:start + self.seq]
        if self.mode == "discrete":
            index_list = random.sample(list(range(video.shape[0])), self.seq)
            index_list.sort()
            video = video[index_list]
        return video, label

    def __len__(self):
        """Get the the size of dataset."""
        return len(self.path)
